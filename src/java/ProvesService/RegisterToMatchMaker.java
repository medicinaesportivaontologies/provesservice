/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ProvesService;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import uk.ac.bath.cs.alive.MatchMaker;
import uk.ac.bath.cs.alive.MatchMakerWSImpl;

/**
 *
 * @author opengeek
 */

@Singleton
@Startup
public class RegisterToMatchMaker {
    
    @PostConstruct
    public void init() {
        MatchMaker mm = new MatchMaker();
        MatchMakerWSImpl mp = mm.getMatchMakerPort();
        mp.mapDomainOntology("http://medicinaesportivaontologies.bitbucket.org/documents.owl",
                "http://medicinaesportivaontologies.bitbucket.org/documents.owl");
        mp.registerServiceByURL("http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves.owl");
    } 

}
