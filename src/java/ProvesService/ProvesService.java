/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ProvesService;

import javax.jws.WebService;
import org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves.Pacient;
import org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves.PacientType;
import org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves.ProvaType;
import org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves.ProvesPortType;
import org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves.ReservaType;
import org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves.ServeiMedic;

/**
 *
 * @author opengeek
 */
@WebService(serviceName = "ProvesService")
public class ProvesService implements ProvesPortType {

    @Override
    public ReservaType demanarProva(PacientType pacient, ProvaType prova) {
        ReservaType rT = new ReservaType();
        
        Pacient p = new Pacient();
        p.setNom(pacient.getNom() );
        p.setCognom1(pacient.getCognom1() );
        p.setCognom2(pacient.getCognom2() );
        p.setDni(pacient.getDni() );
        
        ServeiMedic sm = new ServeiMedic();
        sm.setNom(prova.getNom() );

        rT.setName(p.getDni() + "&" + sm.getNom() );
        rT.getDelSolicitant().add(p);
        rT.getPelServei().add(sm);
        
        return rT;
    }

}
