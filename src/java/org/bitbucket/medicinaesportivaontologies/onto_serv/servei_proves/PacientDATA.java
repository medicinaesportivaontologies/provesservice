
package org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PacientDATA.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PacientDATA">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="pacient_1"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PacientDATA")
@XmlEnum
public enum PacientDATA {

    @XmlEnumValue("pacient_1")
    PACIENT_1("pacient_1");
    private final String value;

    PacientDATA(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PacientDATA fromValue(String v) {
        for (PacientDATA c: PacientDATA.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
