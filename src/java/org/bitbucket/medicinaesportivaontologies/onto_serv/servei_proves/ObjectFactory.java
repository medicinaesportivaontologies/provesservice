
package org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Prova_QNAME = new QName("http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves", "Prova");
    private final static QName _Pacient_QNAME = new QName("http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves", "Pacient");
    private final static QName _Reserva_QNAME = new QName("http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves", "Reserva");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReservaType }
     * 
     */
    public ReservaType createReservaType() {
        return new ReservaType();
    }

    /**
     * Create an instance of {@link PacientType }
     * 
     */
    public PacientType createPacientType() {
        return new PacientType();
    }

    /**
     * Create an instance of {@link ProvaType }
     * 
     */
    public ProvaType createProvaType() {
        return new ProvaType();
    }

    /**
     * Create an instance of {@link ServeiMedic }
     * 
     */
    public ServeiMedic createServeiMedic() {
        return new ServeiMedic();
    }

    /**
     * Create an instance of {@link Pacient }
     * 
     */
    public Pacient createPacient() {
        return new Pacient();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProvaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves", name = "Prova")
    public JAXBElement<ProvaType> createProva(ProvaType value) {
        return new JAXBElement<ProvaType>(_Prova_QNAME, ProvaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PacientType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves", name = "Pacient")
    public JAXBElement<PacientType> createPacient(PacientType value) {
        return new JAXBElement<PacientType>(_Pacient_QNAME, PacientType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves", name = "Reserva")
    public JAXBElement<ReservaType> createReserva(ReservaType value) {
        return new JAXBElement<ReservaType>(_Reserva_QNAME, ReservaType.class, null, value);
    }

}
