
package org.bitbucket.medicinaesportivaontologies.onto_serv.servei_proves;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProvaType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProvaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="provaID" type="{http://medicinaesportivaontologies.bitbucket.org/onto-serv/servei_proves}ProvaDATA"/>
 *         &lt;sequence>
 *           &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProvaType", propOrder = {
    "provaID",
    "name",
    "nom"
})
public class ProvaType {

    protected ProvaDATA provaID;
    protected String name;
    protected String nom;

    /**
     * Gets the value of the provaID property.
     * 
     * @return
     *     possible object is
     *     {@link ProvaDATA }
     *     
     */
    public ProvaDATA getProvaID() {
        return provaID;
    }

    /**
     * Sets the value of the provaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvaDATA }
     *     
     */
    public void setProvaID(ProvaDATA value) {
        this.provaID = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the nom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets the value of the nom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom(String value) {
        this.nom = value;
    }

}
